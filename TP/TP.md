---
title: TP IA sur la connaissance d'Asterix
geometry: "left=0.5cm,right=0.5cm,top=0cm,bottom=0cm"
---

<!-- pandoc -t latex  -s -f markdown -V --mathjax --pdf-engine=xelatex TP.md -->

# Base d'apprentissage

1. Faire une vidéo de chaque figurine de plusieurs minutes
2. Convertir la video en images, par exemple avec ffmpeg: ``ffmpeg -i obelix.MOV -r 10 -vf scale=150:150 obelix%03d.jpg``
3. Faire un dossier pour **Obelix** et un dossier pour **Asterix**
4. Convertir les images en images à niveau de gris dans un autre dossier (bonus).

### A rendre :

- Un dossier ``Data``  comprenant le dossier avec les images d'Asterix le dossier avec les images d'Obelix (**3 pts**)
- Un sous-dossier comprenant les mêmes images mais  *en niveau de gris* (1 pts de bonus)

# Apprentissage

- Entraîner le réseau de neurones :
   1. sans augmentation de données
   2. avec augmentation de données
   3. avec images à niveau de gris (bonus)

- Pour chaque entraînement, tracer les courbes d'apprentissage.
- Régler les hyper-paramètre pour éviter sur-ajustement/sous-ajustement et assurer une convergence de l'apprentissage.

### A rendre :
- Dans le Jupyter Notebook :
   - sans augmentation de données
      1. Le découpage des données (**2 pts**)
      2. La construction de l'architecture (**2 pts**)
      3. L'apprentissage avec les courbes d'apprentissage  (**2 pts**)
   - avec augmentation de données
      1. L'augmentation des données (**2 pts**)
      2. L'apprentissage avec les courbes d'apprentissage  (**2 pts**)
   - avec images à niveau de gris (bonus)
      - Le pipeline complet (3 pts de bonus)
- Les modèles avec les poids du réseau au format h5 (**1 pts**)

# Tests
1. Prendre 10 nouvelles photos d'Asterix et d'Obelix **pendant le TP** en variant le fond pour chacune des 10 images.
2. Tester vos modèles sur les 10 images en utilisant un tableau et des boucles.

###  A rendre :
-  Le code des tests effectués (**3 pts**)
- Les 10 photos dans un dossier ``Test`` avec un dossier Asterix et un dossier Obelix (**1 pts**)

# Mise en production

Créer une appli web pour reconnaître Asterix ou Obélix.

###  A rendre :
- Le code de l'appli web (html, css et js) (**1 pts**)
- Le modèle utilisé par l'appli web  (**1 pts**)






