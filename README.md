---
author: PF Villard
title: Deep Asterix
---

L'objectif est d'apprendre à utiliser des outils d'intelligence artificielle pour reconnaître si une photo représente Obelix ou Asterix. La technique est plus spécialement l'utilisation d'un **réseau neuronal convolutif** (CNN).

# Préparation du matériel

## Impression

![](3D/3D.jpg)

Plusieurs figurines représentant    Asterix ou Obelix on été imprimées en 3D. Les fichiers se trouvent [ici](3D/Asterix.stl) et [ici](3D/Obelix.stl).

![Vidéo de l'impression](videos/3Dprint2.mp4)

## Peinture

| ![](paint/im0.jpg) | ![](paint/im2.jpg)  | ![](paint/im8.jpg)  | ![](paint/im12.jpg)  |
| :------------------: | :-------------------: | :-----------------: | :-------: |

La liste des peintures utilisées est [ici](paint/readme.md#Peintures).

Quelques photos supplémentaires se trouvent [ici](paint/readme.md#Photos).



## Modèle 3D texturé

Utilisation de la méthode de dépliage UV :

![](3D/uv.png)

Résultat [ici](https://members.loria.fr/PFVillard/files/asterix/asterix.html)

# Apprentissage

## Base d'apprentissage

1. Faire une vidéo de chaque figurine de plusieurs minutes
2. Convertir la video en images, par exemple avec (ffmpeg)[https://ffmpeg.org] :
```
ffmpeg -i obelix.MOV -r 10    -vf scale=150:150 obelix/obelix%03d.jpg
```
avec :
   - ``obelix.MOV`` : le fichier video
   - ``10`` : la fréquence pour récupérer une image
   - ``150:150``la taille de l'image
   - ``obelix/obelix%03d.jpg``la forme du fichier image en sortie

![](images/dossiers.jpg)

3. Faire un dossier pour **Obelix** et un dossier pour **Asterix**

![Exemple de vidéo courte](videos/asterixShort.mp4)

# Exercices

1. Exportations des données d'entrainement en utilisant une vidéo et en la convertissant en photos. La vidéo doit comprendre des rotations et translations.
2. Utilisation d'images de synthèse en webGL pour générer des données avec différentes translations, rotations, positions de sources de lumières, etc.
3. Entrainement du réseau de neurone :
   - sans augmentation de données
   - avec augmentation de données
   - avec utilisation d'images de synthèses
4. Verification de l'importance de la couleur : vérifier si la méthode marche aussi bien avec des images en niveau de gris
5. Essai avec des images de personne
6. Essai avec d'autres classes

## Exemple de résultat

| Image à tester | Résultat |
| :------------------: | :-------------------: | 
| ![](images/asterix082.jpg)| Asterix |
| ![](images/obelix008.jpg)| Obelix |
| ![](images/asterix140.jpg)| Asterix |
| ![](images/obelix109.jpg)| Obelix |

## Application

Lien : [ici](https://members.loria.fr/PFVillard/files/asterix/)
